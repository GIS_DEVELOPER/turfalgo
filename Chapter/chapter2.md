# cleanCoords

清除几何要素中多余的点，这里的多余点有两种情况：

* 坐标相同的点
* 坐标不相同，但是这个点在前后两个点的连线上

思路：不管是点、线、面，基本的组成单元都是点的集合，所以可以通过遍历点的集合来排除所有重复的点

# flip

翻转x, y 坐标

# rewind

重绕几何要素，在geojson中几何要素的记录方式有约定的方向，通常是外环逆时针，内环顺时针。这里说的重绕即是将要素的记录方式改变。

这里以一个Polygon举例

思路：

1、判断这个Polygon的旋转方向

```javascript
// 判断LineString的旋转方向
export default function booleanClockwise(line: Feature<LineString> | LineString | Position[]): boolean {
    // 获取line的坐标数组
    const ring = getCoords(line);
    let sum = 0;
    let i = 1;
    let prev;
    let cur;
    
    // 遍历所有的节点，累加（x1 - x0）* (y1 + y0),   判断累加器的结果
    while (i < ring.length) {
        prev = cur || ring[0];
        cur = ring[i];
        sum += ((cur[0] - prev[0]) * (cur[1] + prev[1]));
        i++;
    }
    
   	// 结果大于0，则为顺时针
    return sum > 0;
}
```



2、更改这个Polygon的坐标数组

```javascript
function rewindPolygon(coords, reverse) {
    // 外环
    if (booleanClockwise(coords[0]) !== reverse) {
        coords[0].reverse();
    }
    // 内环
    for (var i = 1; i < coords.length; i++) {
        if (booleanClockwise(coords[i]) === reverse) {
            coords[i].reverse();
        }
    }
}
```



# round

坐标数组取特定位数，参考Math.round()

# truncate

坐标数组降低精度