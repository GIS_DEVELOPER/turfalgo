# booleanClockwise

判断多边形的坐标是否是顺时针排列

假设多边形的坐标为：(X1,Y1), (X2, Y2), (X3, Y3), ... , (Xn, Yn),其中(X1, Y1)与(Xn, Yn)坐标相同，保证多边形闭合

则根据公式：

![](../Image/booleanClockwise.png)

结果N为正则表示顺时针，负表示逆时针

# booleanDisjoint

判断两个几何要素是否分离，即两个几何要素没有相交点。有如下这些情况

| 相交类型 | 算法描述                           |
| -------- | ---------------------------------- |
| 点-点    | 判断点是否相等                     |
| 点-线    | 判断点是否在线上                   |
| 点-面    | 判断点是否在面内                   |
| 线-面    | 线由点构成，可等价于判断点在面内   |
| 面-面    | 面由线构成，也可等价于判断点在面内 |

从上表可知，判断两个要素是否分离，主要有两个算法：

* 点在线上

  参考【booleanPointOnLine】

* 点在面内

  参考【booleanPointInPolygon】

# booleanOverlap

判断相同个要素是否有相交，有如下情况：

| 类型                            | 说明                             |
| ------------------------------- | -------------------------------- |
| MultiPoint之间                  | 判断是否存在相等的点             |
| LineString、MultiLineString之间 | 判断是否存在重叠的线段           |
| Polygon、MultiPolygon之间       | 判断组成多边形的线段是否存在相交 |

点很好判断，下面看两点：

* 线段重叠

  两条线段是否有重叠可理解为：

  1、两条线段共线

  2、线段中的一点在另一条线段范围内

* 线段相交

  参考【lineIntersect】

# booleanParallel

判断两条线段是否平行

思路：获取两条线的方位角度，判断是否相等

# booleanPointInPolygon

判断点是否在Polygon内部

**实现思路：**

1. 遍历Polygon的每条边
2. 判断点是否在该边上，如果是则返回true
3. 判断点是否在该多边形内部，如果是则返回true

多边形可以遍历的部分有节点和边，那么为什么不遍历节点呢，因为判断点是否在多边形内部需要考虑到多边形的边界信息。

**点在边上的判断思路：**

已知该边的两点为(x1, y1),(x2, y2),那么经过这条边的直线L表达式为：

​	(y1 - y1) x + (x1 - x2) y + (x2y1 - x1y2) = 0

点在直线L上即代入点A(x, y)等式成立

然后判断点A如果在[x1, x2], [y1, y2]区间范围内，则点A在线段即这条边上。

**点在多边形内部的判断思路：**

**PNPoly 算法: **以测试点为起点，作一条水平的射线，判断该射线与多边形区域的边的交点个数，如果为奇数个，说明在区域内，否则就在区域外。 为什么能成立呢？可以这样考虑：多边形区域（不考虑有洞的情况）将空间分成了内外两个部分，如果测试点在空间内，那么作一条射线（这里向右），至少会边有一个交点，相交后就到了区域外。 如果再有一个交点，又到了区域内，以此类推。因此奇数个交点说明在区域内，偶数个说明在区域外。

下面看代码实现过程：

```javascript
  let isInside = false;

  for (let i = 0, j = ring.length - 1; i < ring.length; j = i++) {

   // 将各个边缓存下来

    const xi = ring[i][0];

    const yi = ring[i][1];

    const xj = ring[j][0];

    const yj = ring[j][1];

	// 根据上面的思路，射线与线段有交点则射线点的纵坐标在线段两点纵坐标之间

	let f1 = (yi > pt[1]) !== (yj > pt[1]);

	// 根据上面的思路，射线与线段有交点则射线点的横坐标线段所在的直线左边：

	// 直线表达式：y - y1 = (y2 - y1)/(x2 - x1) *(x - x1) (变换可得：x = (y - y1)(x2 - x1)/(y2 - y1) + x1)

	// 点在直线左边：x < (y - y1)(x2 - x1)/(y2 - y1) + x1

	let f2 =  pt[0] < (xj - xi) * (pt[1] - yi) / (yj - yi) + xi;

    const intersect = f1&&f2;

	// 如果两个条件都满足，则说明点与线段有交点，但是不能说明点在多边形内部

	// 这里的isInside有个取反的操作，当有奇数个交点时最终的inInside为true，偶数个交点最终的inside为false

    if (intersect) {

      isInside = !isInside;

    }

  }

  return isInside;
```

# booleanPointOnLine

判断点是否在折线上，点在折线上可以转化为点在是否在线段上，因为折线由线段组成，那么如何判断点P(x, y)是否在折线上呢

**步骤：**

1、使用叉积公式判断点在线段上两点三点共线

> **叉积**（英语：Cross product）又称**向量积**（英语：Vector product），是对三维空间中的两个向量的二元运算，使用符号X。如果两个向量方向相同或相反（即它们没有线性无关的分量），亦或任意一个的长度为零，那么它们的叉积为零。

若给定一点Q(a,b),和线段M的首尾两个端点P1(x1,y1),P2(x2,y2),要求判断点Q否在线段M上；则需满足

​						(a - x1) * (y2 - y1)  - (b - y1) * (x2 - x1) = 0

2、判断点是否在线段所在的横坐标/纵坐标范围内

在满足步骤1的条件下，只需要判断点在线段的横纵标范围内或者是纵坐标范围内即可，不需要两者都判断

# booleanWithin

| 类型     | 描述                                                         |
| -------- | ------------------------------------------------------------ |
| 点在点内 | 判断点是否相等                                               |
| 点在线内 | 参考【booleanPointInLine】                                   |
| 点在面内 | 参考【booleanPointInPolygon】                                |
| 线在线内 | 首先判断两者包络线是否存在包含关系，其次判断一条线上的点是否都在另一条线上 |
| 线在面内 | 首先判断两者包络线是否存在包含关系，其次判断一条线上的点是否都在面内 |
| 面在面内 | 首先判断两者包络线是否存在包含关系，其次判断一个面上的点是否都在另一个面内 |

