# randomPosition

获取bbox内的随机点

```javascript
[
    (Math.random() * (bbox[2] - bbox[0])) + bbox[0],
    (Math.random() * (bbox[3] - bbox[1])) + bbox[1]
];
```

# randomPoint

将randomPosition产生的点转为Point

# randomLineString

思路：

1. 生成一个随机的点P1
2. 给定一个随机距离L和随机角度A，可又P1求出下一个点P2，再根据P2可求出P3

下面给出生成一条随机线的算法：

```javascript
/** 设定折线上面线段的最大长度为max_length，最大的偏转角为max_rotation(相邻两条线段的夹角,逆时针方向)*/
// 生成一个随机的点
const startingPoint = randomPosition(bbox);
const vertices = [startingPoint];
// 循环生成线上所有的随机点
for (let j = 0; j < n - 1; j++) {
    // 如果是第一个点则生成一个随机的初始角度，如果不是则初始角度为线上一条线段的角度
    const priorAngle = (j === 0) ?
          Math.random() * 2 * Math.PI :
    Math.tan(
        (vertices[j][1] - vertices[j - 1][1]) /
        (vertices[j][0] - vertices[j - 1][0]),
    );
    // 初始角度加上随机偏转角
    const angle = priorAngle + (Math.random() - 0.5) * max_rotation * 2;
    // 随机距离
    const distance = Math.random() * max_length;
    // 根据正余弦函数求出该点坐标，这里使用正余弦函数是没有问题的，画出0-2π的图像可以看出
    vertices.push([
        vertices[j][0] + distance * Math.cos(angle),
        vertices[j][1] + distance * Math.sin(angle),
    ]);
}
```



# randomPolygon

生成随机的凸多边形

思路：任何的凸多边形的外角和都是360°，外角可以理解为多边形上相邻线段的偏转角，如果要生成一个凸n变形，可以通过偏转n次角度得到一个多边形。

```javascript
/** 生成一个随机的凸n多边形，多边形边长不超过max_length
 */
let vertices: any[] = [];
// 每个外角的偏转因子
const circleOffsets = Array.apply(null, new Array(n + 1)).map(Math.random);

// 累加外角的偏转，方便三角函数运算
circleOffsets.forEach((cur: any, index: number, arr: any[]) => {
    arr[index] = (index > 0) ? cur + arr[index - 1] : cur;
});

circleOffsets.forEach((cur: any) => {
    cur = cur * 2 * Math.PI / circleOffsets[circleOffsets.length - 1];
    const radialScaler = Math.random();
    vertices.push([
        radialScaler * (max_length || 10) * Math.sin(cur),
        radialScaler * (max_length || 10) * Math.cos(cur),
    ]);
});
vertices[vertices.length - 1] = vertices[0]; // close the ring
```

