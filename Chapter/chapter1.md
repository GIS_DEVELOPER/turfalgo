# distance

根据两点的经纬度坐标计算两点的距离采用的是球面余弦定理和半正矢公式（这里使用的是球面模型）

参考（https://johnnyqian.net/blog/gps-locator/）

这里给出实现过程，公式略微复杂，最好先查看上面的链接看推导过程

```javascript
// 纬度弧度差，用弧度表示
var dLat = degreesToRadians((coordinates2[1] - coordinates1[1]));
// 经度弧度差，用弧度表示
var dLon = degreesToRadians((coordinates2[0] - coordinates1[0]));

// 起点纬度，用弧度表示
var lat1 = degreesToRadians(coordinates1[1]);
// 终点纬度，用弧度表示                                                                                 
var lat2 = degreesToRadians(coordinates2[1]);

// 这里的a根据球面余弦定理和半正矢公式推算，表示sin2（@/2），@表示两点的球心角
var a = Math.pow(Math.sin(dLat / 2), 2) +
    Math.pow(Math.sin(dLon / 2), 2) * Math.cos(lat1) * Math.cos(lat2);

// 这里的b表示两点的球心角大小
var b = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

// 通过弧长公式计算两点的大圆弧长，即两点之间的距离
// 地球半径为6371008.8m
return radiansToLength(b);
```

**弧长公式**

L = a * r

L为弧长，a为角度(弧度表示)，r为圆的半径

**度转弧度**

a = d * pi / 180

a为弧度值， d为角度，pi为圆周率

# bearing

`方位角`是从某点的指北经线起，依顺时针方向到目标方向线之间的水平夹角（如图所示θ，可以将其看成是指南针所指示的角度），也即是OPN平面与OPQ平面的所构成的`二面角`大小。公式使用三面角的正弦定理和三面角的余弦定理推导，这里给出最终的结果公式，详情参考(链接无法打开请访问源码，有离线网页)：https://johnnyqian.net/blog/gps-locator/

球模型：

![](../Image/bearing1.png)

最终公式：

![](../Image/bearing2.png)

下面通过javascript实现

 ```javascript
// 起点经度，弧度表示
const lon1 = degreesToRadians(coordinates1[0]);
// 起点纬度，弧度表示
const lon2 = degreesToRadians(coordinates2[0]);
// 终点经度，弧度表示
const lat1 = degreesToRadians(coordinates1[1]);
// 终点纬度，弧度表示
const lat2 = degreesToRadians(coordinates2[1]);

// a，b为推导的中间过程
const a = Math.sin(lon2 - lon1) * Math.cos(lat2);
const b = Math.cos(lat1) * Math.sin(lat2) -
      Math.sin(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1);

return radiansToDegrees(Math.atan2(a, b));
 ```

# along

获取沿着线段一段一定距离的点坐标

**实现思路**

* 遍历线段节点数组，设置变量保存遍历的距离
  * 如果给出的距离超出线段长度，则返回最后一个节点
  * 如果给出的距离没有超出线段长度
    * 遍历的距离小于给定长度，则继续遍历
    * 遍历的距离等于给定长度，返回该节点
    * 遍历的距离大于给定长度，则减去超出的部分，返回节点

```javascript
// 获取geojson中线段的节点坐标数组
const geom = getGeom(line);
const coords = geom.coordinates;
// 保存遍历的距离
let travelled = 0;
// 遍历坐标数组
for (let i = 0; i < coords.length; i++) {
  	// 给定距离大于线段长度
    if (distance >= travelled && i === coords.length - 1) { 
      break;
    } 
    // 遍历的距离大于等于线段长度
  	else if (travelled >= distance) {
      const overshot = distance - travelled;
      // 遍历的距离等于线段长度
      if (!overshot) { 
        return point(coords[i]);
      } 
      // 遍历的距离大于线段的长度，则截取
      else {
        // bearing 为计算两点的角度
        const direction = bearing(coords[i], coords[i - 1]) - 180;
        // destination为在给定点一定角度和和距离的一个点
        const interpolated = destination(coords[i], overshot, direction);
        return interpolated;
      }
    }
   // 遍历的距离小于线段长度
  	else {
        travelled += distance(coords[i], coords[i + 1]);
    }
}
return coords[coords.length - 1];
```

# area

> GeoJSON格式的polygon是三维数组表示，其实如果仅仅保存多边形的各个节点的坐标只需要二维数组就可以了，为什么还需要有三维呢？这是因为镂空多边形的存在，就是多边形不是实心的，里面有一个多边形的区域不存在。在这个三维数组的第一维中一般是一个数组，如果出现多个数组就说明有镂空多边形的存在，第一个数组是外多边形，后面的数组是内多边形。

```json
{
    "type": "Polygon",
    "coordinates": [
        [
            [41, 55],
            [95, 55],
            [95, 33],
            [41, 33],
            [41, 55]
        ]	
    ]
}
// 上面是一个简单的Polygon
// 下面是一个镂空的多边形
{
    "type": "Polygon",
    "coordinates": [
        [
            [41, 55],
            [95, 55],
            [95, 33],
            [41, 33],
            [41, 55]
        ],
        [
            [54, 40],
            [68, 44],
            [80, 40],
            [54, 40]
        ]
    ]
}
```



计算多边形面积公式：

​	S = S1 - （S2 + S3 + ... + Sn）;

S为多边形的面积，S1为最外面的多边形面积，在GeoJSON中对应coordinates数组的第一个元素，S2到Sn是内部的多边形面积，在GeoJSON中对应coordinates数组的第二个到第n个元素。

可以发现，计算镂空多边形的面积可以分解为计算简单多边形的面积，这里给出简单多边形面积计算方法。

计算公式

![面积计算公式](../Image/area.png)

公式中t表示纬度，n表示经度，R（这里取6378137m）是地球半径，m是多边形节点的个数

举个栗子：

```json
// 一个多边形：
{
    "type": "Feature",
    "geometry": {
        "type": "Polygon",
        "coordinates": [
            [
                [n1, t1],
                [n2, t2],
                [n3, t3],
                [n4, t4]
            ]
        ]
    }
}
// 那么它的面积为：
// S = [(n2 - n4) * sint1 + (n3 - n1) * sint2 + (n4 - n2) * sint3 + (n1 - n3) * sint4] * R * R / 2;
```

# bbox

获取几何要素（线、面）的范围数据，结果为数组[ minX, minY, maxX, maxY]

实现思路：

* 声明变量，保存结果值
* 遍历几何要素节点，与变量比较，更新变量

实现过程

```javascript
// 声明变量，Infinity表示无穷大值
const result = [Infinity, Infinity, -Infinity, -Infinity];
// 遍历几何要素节点数组
arr.forEach( coord => {
    // 节点的经度、纬度小于变量
    if (result[0] > coord[0]) { result[0] = coord[0]; }
    if (result[1] > coord[1]) { result[1] = coord[1]; }
    // 节点的经度、纬度大于变量
    if (result[2] < coord[0]) { result[2] = coord[0]; }
    if (result[3] < coord[1]) { result[3] = coord[1]; }
});
return result;
```

# center

获取多边形包含块中心点坐标。

在获取bbox的基础上通过对经纬的取平均值可得到中心点坐标，参考bbox的算法。

算法：

bbox结果为： [minX, minY, maxX, maxY]

center的经度：lon = （minX+ maxX）/ 2;

center的纬度：lat = （minY + maxY）/ 2;

# centerid

> centerid与center的区别：
>
> 1、衡量一个多边形中心点的不同尺度
>
> 2、都存在点不在多边形内部的情况

获取多边形所有节点中心点坐标。

获取所有多边形节点横纵坐标平均值，公式为

![image-20200616135735270](../Image/Snipaste_2020-06-16_14-00-20.png)

Xi,Yi为坐标点的经度、纬度，X为centerid经度，Y为centerid纬度

# destination

给定一个点和距离、方位，求另一个点的坐标。

算法采用半正矢公式推导，

```javascript
// 起点经度，弧度表示
const longitude1 = degreesToRadians(coordinates1[0]);
// 起点纬度，弧度表示
const latitude1 = degreesToRadians(coordinates1[1]);
// 以正北为0度，顺时针方向的角度，弧度表示
const bearingRad = degreesToRadians(bearing);
// 相差的距离，弧度表示
const radians = lengthToRadians(distance, options.units);

// 目标点纬度值
const latitude2 = Math.asin(Math.sin(latitude1) * Math.cos(radians) +
                            Math.cos(latitude1) * Math.sin(radians) * Math.cos(bearingRad));

// 目标点经度值
const longitude2 = longitude1 + Math.atan2(Math.sin(bearingRad) * Math.sin(radians) * Math.cos(latitude1),
                                           Math.cos(radians) - Math.sin(latitude1) * Math.sin(latitude2));
const lng = radiansToDegrees(longitude2);
const lat = radiansToDegrees(latitude2);

return point([lng, lat], options.properties);
```

**弧长转为弧度**

a = L / r

a为弧度，L为弧长， r为地球半径

# centerOfMass

返回一个多边形的的质心，这个和center不一样，center指的是指包含多边形最小矩形的中心，而centerOfMass指的是物理意义上的质量集中点。拿一个直角三角形来看，这个三角形的center位于斜边上（各边垂直平分线交点），而centerOfMass位于三角形内部（各边中线交点）。

公式：

多边形n个节点为（x0,y0）,(x1,y1),(x2,y2),...,(xn-1,yn-1), 质心点为(Cx, Cy)

![](../Image/centerofmass1.png)

![centerofmass2](../Image/centerofmass2.png)

A是计算多边形的面积公式，也叫鞋带公式。

# length

获取线段长度

实现过程：

* 遍历线段对象节点
* 累加线段距离，距离公式参考[distance]

# midpoint

计算两个点的中点

中点的计算方法其实只需要取经纬度的平均数就可以了，可是从意义上来说，取平均数的方法是应用在平面几何上，这里使用的方法是这样：

* 获取两点A、B的球面距离distance
* 获取B点相对于A点的方位角bearing
* 使用destination方法带入A点坐标，distance和bearing计算中点

这样使用的球面几何的方法，取得的结果是平均数的近似值。

# pointOnFeature

获取一个在要素内部的点，以Polygon举例。

**实现过程：**

1. 获取Polygon中心点centerid
2. 如果中心点在Polygon内部，则返回中心点
3. 如果中心点不在Polygon内部，则返回与中心点最接近的点

下面逐步看实现过程

1. 参考centerid的获取过程
2. 判断中心点是否在Polygon内部, 参考【booleanPointInPolygon】方法
3. 如果centerid在Polygon内部，则返回centerid
4. 如果centerid不在Polygon内部，则返回Polygon与centerid最接近的点，参考【nearestPoint】方法

# polygonTangents

获取多边形相对于多边形外的点的切点集合



# pointToLineDistance

获取点到线的距离，这里的线指的是具有线结构的feature,可以是LineString也可以是Polygon。这里一条折线段考虑，获取折线段外一点A到这条折线段L的最短距离。

思路：获取折线段的最短距离，可以比较点A与L上各个线段的距离，取最小的值即可。如何获取点到线段的距离呢？我们先看看点在线段外的三种情况：
![](../Image/Snipaste_2020-06-18_16-08-19.png)

* 第一种情况G点到线AB的最短距离为AG, 夹角区间[90 180]
* 第二种情况H点到线CD的最短距离为DH, 夹角区间[0 90)
* 第三种情况I点到线EF的最短距离为I点到线EF的垂线，夹角区间为[0, 90)

解决这个最短距离的问题有两种：1）解析几何   2） 向量

通过解析几何的方法较为繁琐，而且程序运行的开销较大，这里用向量解决, G（x, y），A(x1,  y1)，B(x2, y2)三点坐标已知。

第一种情况:

AB向量与AG向量的数乘不大于0

(x - x1) * (x2 - x1) + (y - y1)*(y2 - y1) <= 0;

第二种情况：

AB向量与AG向量的数乘不小于AB向量的自乘，推导不难

(x - x1) * (x2 - x1) + (y - y1)*(y2 - y1 >= (x2 - x1)(x2 - x1) + (y2 - y1)(y2 - y1)

第三种情况：

除了第一、二种情况即为第三种情况

垂线的长度可通过解析几何计算，也可通过向量计算

# rhumbBearing

> 理解恒向角之前需要先了解大圆航线和恒向航线，大圆航线是两点之间距离最近的航线，但是一般情况下航线与经线角度是不断变化的（经线圈和纬线圈除外），这就意味着如果用罗盘导航的话（罗盘北方向与经线近似一致）就需要不停地变换航向。恒向航线里面的这个恒向指的是罗盘上的指针是不变的，也就是说航线与经线的夹角一致。其实恒向航线其实是一条曲线，而大圆航线可以看做一条直线，有这个区分是指用罗盘来导航来说的，如果现代的GPS定位就只需要用大圆航线这条最短路线了。

获取恒向角，即是恒向航线时航线与经度的夹角。

// TODO: 待完善

```javascript
function calculateRhumbBearing(from: number[], to: number[]) {
    // φ => phi
    // Δλ => deltaLambda
    // Δψ => deltaPsi
    // θ => theta
    const phi1 = degreesToRadians(from[1]);
    const phi2 = degreesToRadians(to[1]);
    let deltaLambda = degreesToRadians((to[0] - from[0]));
    // if deltaLambdaon over 180° take shorter rhumb line across the anti-meridian:
    if (deltaLambda > Math.PI) { deltaLambda -= 2 * Math.PI; }
    if (deltaLambda < -Math.PI) { deltaLambda += 2 * Math.PI; }

    const deltaPsi = Math.log(Math.tan(phi2 / 2 + Math.PI / 4) / Math.tan(phi1 / 2 + Math.PI / 4));

    const theta = Math.atan2(deltaLambda, deltaPsi);

    return (radiansToDegrees(theta) + 360) % 360;
}
```



# rhumbDestination

给定一个点和距离和罗盘方位角，求另一个点

// TODO:待完善

```javascript
/**
 * Returns the destination point having travelled along a rhumb line from origin point the given
 * distance on the  given bearing.
 * Adapted from Geodesy: http://www.movable-type.co.uk/scripts/latlong.html#rhumblines
 *
 * @private
 * @param   {Array<number>} origin - point
 * @param   {number} distance - Distance travelled, in same units as earth radius (default: metres).
 * @param   {number} bearing - Bearing in degrees from north.
 * @param   {number} [radius=6371e3] - (Mean) radius of earth (defaults to radius in metres).
 * @returns {Array<number>} Destination point.
 */
function calculateRhumbDestination(origin: number[], distance: number, bearing: number, radius?: number) {
    // φ => phi
    // λ => lambda
    // ψ => psi
    // Δ => Delta
    // δ => delta
    // θ => theta

    radius = (radius === undefined) ? earthRadius : Number(radius);

    const delta = distance / radius; // angular distance in radians
    const lambda1 = origin[0] * Math.PI / 180; // to radians, but without normalize to 𝜋
    const phi1 = degreesToRadians(origin[1]);
    const theta = degreesToRadians(bearing);

    const DeltaPhi = delta * Math.cos(theta);
    let phi2 = phi1 + DeltaPhi;

    // check for some daft bugger going past the pole, normalise latitude if so
    if (Math.abs(phi2) > Math.PI / 2) { phi2 = phi2 > 0 ? Math.PI - phi2 : -Math.PI - phi2; }

    const DeltaPsi = Math.log(Math.tan(phi2 / 2 + Math.PI / 4) / Math.tan(phi1 / 2 + Math.PI / 4));
    // E-W course becomes ill-conditioned with 0/0
    const q = Math.abs(DeltaPsi) > 10e-12 ? DeltaPhi / DeltaPsi : Math.cos(phi1);

    const DeltaLambda = delta * Math.sin(theta) / q;
    const lambda2 = lambda1 + DeltaLambda;

    return [((lambda2 * 180 / Math.PI) + 540) % 360 - 180, phi2 * 180 / Math.PI]; // normalise to −180..+180°
}
```



# rhumbDistance

获取罗盘方位距离

// TODO: 待完善

```javascript
/**
 * Returns the distance travelling from ‘this’ point to destination point along a rhumb line.
 * Adapted from Geodesy: https://github.com/chrisveness/geodesy/blob/master/latlon-spherical.js
 *
 * @private
 * @param   {Array<number>} origin point.
 * @param   {Array<number>} destination point.
 * @param   {number} [radius=6371e3] - (Mean) radius of earth (defaults to radius in metres).
 * @returns {number} Distance in km between this point and destination point (same units as radius).
 *
 * @example
 *     var p1 = new LatLon(51.127, 1.338);
 *     var p2 = new LatLon(50.964, 1.853);
 *     var d = p1.distanceTo(p2); // 40.31 km
 */
function calculateRhumbDistance(origin: number[], destination: number[], radius?: number) {
    // φ => phi
    // λ => lambda
    // ψ => psi
    // Δ => Delta
    // δ => delta
    // θ => theta

    radius = (radius === undefined) ? earthRadius : Number(radius);
    // see www.edwilliams.org/avform.htm#Rhumb

    const R = radius;
    const phi1 = origin[1] * Math.PI / 180;
    const phi2 = destination[1] * Math.PI / 180;
    const DeltaPhi = phi2 - phi1;
    let DeltaLambda = Math.abs(destination[0] - origin[0]) * Math.PI / 180;
    // if dLon over 180° take shorter rhumb line across the anti-meridian:
    if (DeltaLambda > Math.PI) { DeltaLambda -= 2 * Math.PI; }

    // on Mercator projection, longitude distances shrink by latitude; q is the 'stretch factor'
    // q becomes ill-conditioned along E-W line (0/0); use empirical tolerance to avoid it
    const DeltaPsi = Math.log(Math.tan(phi2 / 2 + Math.PI / 4) / Math.tan(phi1 / 2 + Math.PI / 4));
    const q = Math.abs(DeltaPsi) > 10e-12 ? DeltaPhi / DeltaPsi : Math.cos(phi1);

    // distance is pythagoras on 'stretched' Mercator projection
    const delta = Math.sqrt(DeltaPhi * DeltaPhi + q * q * DeltaLambda * DeltaLambda); // angular distance in radians
    const dist = delta * R;

    return dist;
}
```



# square

获取包含块的最小正方形包含块

**思路：**

1、判断包含块是竖直还是水平的

2、包含块是竖直的则从中心延伸水平位置，延伸到竖直长度的一半

3、包含块是水平的则从中心延伸竖直位置，延伸到水平长度的一半

```javascript
function square(bbox) {
    var west = bbox[0];
    var south = bbox[1];
    var east = bbox[2];
    var north = bbox[3];

    var horizontalDistance = distance(bbox.slice(0, 2), [east, south]);
    var verticalDistance = distance(bbox.slice(0, 2), [west, north]);
    // 水平摆放
    if (horizontalDistance >= verticalDistance) {
        var verticalMidpoint = (south + north) / 2;
        return [
            west,
            verticalMidpoint - ((east - west) / 2),
            east,
            verticalMidpoint + ((east - west) / 2)
        ];
    } 
    // 竖直摆放
    else {
        var horizontalMidpoint = (west + east) / 2;
        return [
            horizontalMidpoint - ((north - south) / 2),
            south,
            horizontalMidpoint + ((north - south) / 2),
            north
        ];
    }
}
```



# greatCircle

返回两点间的大圆路径

// TODO: 待完成

```javascript
/*
 * http://en.wikipedia.org/wiki/Great-circle_distance
 *
 */
var GreatCircle = function (start, end, properties) {
    if (!start || start.x === undefined || start.y === undefined) {
        throw new Error('GreatCircle constructor expects two args: start and end objects with x and y properties');
    }
    if (!end || end.x === undefined || end.y === undefined) {
        throw new Error('GreatCircle constructor expects two args: start and end objects with x and y properties');
    }
    this.start = new Coord(start.x, start.y);
    this.end = new Coord(end.x, end.y);
    this.properties = properties || {};

    var w = this.start.x - this.end.x;
    var h = this.start.y - this.end.y;
    var z = Math.pow(Math.sin(h / 2.0), 2) +
                Math.cos(this.start.y) *
                   Math.cos(this.end.y) *
                     Math.pow(Math.sin(w / 2.0), 2);
    this.g = 2.0 * Math.asin(Math.sqrt(z));

    if (this.g === Math.PI) {
        throw new Error('it appears ' + start.view() + ' and ' + end.view() + ' are \'antipodal\', e.g diametrically opposite, thus there is no single route but rather infinite');
    } else if (isNaN(this.g)) {
        throw new Error('could not calculate great circle between ' + start + ' and ' + end);
    }
};

/*
 * Generate points along the great circle
 */
GreatCircle.prototype.Arc = function (npoints, options) {
    var first_pass = [];
    if (!npoints || npoints <= 2) {
        first_pass.push([this.start.lon, this.start.lat]);
        first_pass.push([this.end.lon, this.end.lat]);
    } else {
        var delta = 1.0 / (npoints - 1);
        for (var i = 0; i < npoints; ++i) {
            var step = delta * i;
            var pair = this.interpolate(step);
            first_pass.push(pair);
        }
    }
    /* partial port of dateline handling from:
      gdal/ogr/ogrgeometryfactory.cpp

      TODO - does not handle all wrapping scenarios yet
    */
    var bHasBigDiff = false;
    var dfMaxSmallDiffLong = 0;
    // from http://www.gdal.org/ogr2ogr.html
    // -datelineoffset:
    // (starting with GDAL 1.10) offset from dateline in degrees (default long. = +/- 10deg, geometries within 170deg to -170deg will be splited)
    var dfDateLineOffset = options && options.offset ? options.offset : 10;
    var dfLeftBorderX = 180 - dfDateLineOffset;
    var dfRightBorderX = -180 + dfDateLineOffset;
    var dfDiffSpace = 360 - dfDateLineOffset;

    // https://github.com/OSGeo/gdal/blob/7bfb9c452a59aac958bff0c8386b891edf8154ca/gdal/ogr/ogrgeometryfactory.cpp#L2342
    for (var j = 1; j < first_pass.length; ++j) {
        var dfPrevX = first_pass[j - 1][0];
        var dfX = first_pass[j][0];
        var dfDiffLong = Math.abs(dfX - dfPrevX);
        if (dfDiffLong > dfDiffSpace &&
            ((dfX > dfLeftBorderX && dfPrevX < dfRightBorderX) || (dfPrevX > dfLeftBorderX && dfX < dfRightBorderX))) {
            bHasBigDiff = true;
        } else if (dfDiffLong > dfMaxSmallDiffLong) {
            dfMaxSmallDiffLong = dfDiffLong;
        }
    }

    var poMulti = [];
    if (bHasBigDiff && dfMaxSmallDiffLong < dfDateLineOffset) {
        var poNewLS = [];
        poMulti.push(poNewLS);
        for (var k = 0; k < first_pass.length; ++k) {
            var dfX0 = parseFloat(first_pass[k][0]);
            if (k > 0 &&  Math.abs(dfX0 - first_pass[k - 1][0]) > dfDiffSpace) {
                var dfX1 = parseFloat(first_pass[k - 1][0]);
                var dfY1 = parseFloat(first_pass[k - 1][1]);
                var dfX2 = parseFloat(first_pass[k][0]);
                var dfY2 = parseFloat(first_pass[k][1]);
                if (dfX1 > -180 && dfX1 < dfRightBorderX && dfX2 === 180 &&
                    k + 1 < first_pass.length &&
                   first_pass[k - 1][0] > -180 && first_pass[k - 1][0] < dfRightBorderX) {
                    poNewLS.push([-180, first_pass[k][1]]);
                    k++;
                    poNewLS.push([first_pass[k][0], first_pass[k][1]]);
                    continue;
                } else if (dfX1 > dfLeftBorderX && dfX1 < 180 && dfX2 === -180 &&
                     k + 1 < first_pass.length &&
                     first_pass[k - 1][0] > dfLeftBorderX && first_pass[k - 1][0] < 180) {
                    poNewLS.push([180, first_pass[k][1]]);
                    k++;
                    poNewLS.push([first_pass[k][0], first_pass[k][1]]);
                    continue;
                }

                if (dfX1 < dfRightBorderX && dfX2 > dfLeftBorderX) {
                    // swap dfX1, dfX2
                    var tmpX = dfX1;
                    dfX1 = dfX2;
                    dfX2 = tmpX;
                    // swap dfY1, dfY2
                    var tmpY = dfY1;
                    dfY1 = dfY2;
                    dfY2 = tmpY;
                }
                if (dfX1 > dfLeftBorderX && dfX2 < dfRightBorderX) {
                    dfX2 += 360;
                }

                if (dfX1 <= 180 && dfX2 >= 180 && dfX1 < dfX2) {
                    var dfRatio = (180 - dfX1) / (dfX2 - dfX1);
                    var dfY = dfRatio * dfY2 + (1 - dfRatio) * dfY1;
                    poNewLS.push([first_pass[k - 1][0] > dfLeftBorderX ? 180 : -180, dfY]);
                    poNewLS = [];
                    poNewLS.push([first_pass[k - 1][0] > dfLeftBorderX ? -180 : 180, dfY]);
                    poMulti.push(poNewLS);
                } else {
                    poNewLS = [];
                    poMulti.push(poNewLS);
                }
                poNewLS.push([dfX0, first_pass[k][1]]);
            } else {
                poNewLS.push([first_pass[k][0], first_pass[k][1]]);
            }
        }
    } else {
        // add normally
        var poNewLS0 = [];
        poMulti.push(poNewLS0);
        for (var l = 0; l < first_pass.length; ++l) {
            poNewLS0.push([first_pass[l][0], first_pass[l][1]]);
        }
    }

    var arc = new Arc(this.properties);
    for (var m = 0; m < poMulti.length; ++m) {
        var line = new LineString();
        arc.geometries.push(line);
        var points = poMulti[m];
        for (var j0 = 0; j0 < points.length; ++j0) {
            line.move_to(points[j0]);
        }
    }
    return arc;
};
```

