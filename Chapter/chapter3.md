

# bboxClip

使用bbox裁剪多边形，就是通过输入（minX, minY,  maxX, maxY）来裁剪多边形。

**Sutherland–Hodgman算法**

# bezierSpline

将折线贝塞尔化

首先理解贝塞尔曲线，贝塞尔曲线主要用于二维图形应用程序中的数学曲线，曲线由起始点，终止点（也称锚点）和控制点组成，通过调整控制点，通过一定方式绘制的贝塞尔曲线形状会发生变化。

**根据控制点绘制贝塞尔曲线的实现过程：**

1. 四个控制点Po,P1,P2,P3通过先后顺序进行连接，形成了三条线段，然后通过一个参数t, t属于[0 , 1].该参数的值等于线段上某一个点距离起点的长度除以线段长度。就比如线段上有一个点Po’，此时t的值就是 PoPo‘/PoP1，其中Po‘位置如下图所示。

   ![](../Image/beisail1.png)

2. 接下来对每一条线段做同样的操作，得到三个控制点Po', P1',P2'，如下图所示。

![](../Image/beisail2.png)

3. 然后对这三个控制点重复第1步操作，得出两个控制点Po'',P1'',如下图所示。

   ![](../Image/beisail3.png)

4. 最后再使用同样的方法可以得到，最终的一个点Po''',如下图所示，此时这个点就是贝塞尔曲线上的一个点。

   ![](../Image/beisail4.png)

   通过控制t的值，由0增加到1，就绘制了一条曲线

   ![](../Image/beisail5.png)

**贝塞尔曲线的函数：**

> 按照控制点的多少，贝塞尔曲线的方程分为一阶，二阶，三阶和多阶方程，分别对应控制点的个数为2,3,4,5.
>
> 上面给出的曲线就是三阶方程，因为有四个控制点。我们这里用的是三阶方程。

推导过程请参考[深入理解贝塞尔曲线](https://juejin.im/post/5b854e1451882542fe28a53d )，如果链接失效请访问源码,这里直接给出三阶贝塞尔曲线的方程。

![](../Image/hanshu3_1.png)

推导得到的结果为：

![](../Image/hanshu3_2.svg)

点Po,P1,P2,P3是一个个点，意味着将每个点的横坐标或是纵坐标带进去都是可以的。

**根据给定的的折线求贝塞尔曲线：**

思路：曲线上有无数个点，我们无法都取到，只能取有限个点，获取一个近似曲线的坐标数组。观察曲线方程就可以发现，根据给定的控制点有唯一一条曲线，而这条曲线的唯一变量就是t，所以我们可以控制t的值来获取曲线上对应的点。

# buffer

> 缓冲区分析是指以点、线、面实体为基础，自动建立其周围一定宽度范围内的缓冲区[多边形](https://baike.baidu.com/item/多边形/10076694)[图层](https://baike.baidu.com/item/图层/1494948)，然后建立该图层与目标图层的叠加，进行分析而得到所需结果。它是用来解决[邻近度](https://baike.baidu.com/item/邻近度/4830086)问题的空间分析工具之一。邻近度描述了地理空间中两个地物距离相近的程度。

**实现思路：**

* 点：确定中心点——以中心点为圆心、 R为半径生成一个圆——得到缓冲区边界；

* 线、面：确定轴线——以距离 R生成中心轴线的平行曲线——处理转角弧段——对生成的弧段进行求交、合并运算——生成缓冲区边界；

点的缓冲区是最简单的，涉及到面和线的时候，对于拐角的处理有以下两种方式：

**简单平行线法**

思想：在轴线的两边作出平行线，在转角处形成尖角，两端形成弧段，组成缓冲区。

**凸角圆弧法**

思想：与简单平行线不同之处在于，在转角外侧用圆弧来代替尖角，内侧仍然使用尖角的方法，生成缓冲区。turf使用的是这种方式。

这里给出网上找到的求解步骤：

1. 直线性判断，判断相邻三点是否在同一直线上； 
2. 折点凸凹性判断，确定转角的地方哪侧使用直线求交，哪侧使用圆弧连接； 
3. 凸点圆弧的嵌入，即将转角外侧形成的圆弧和两边的线段相连； 
4. 边线关系的判别与处理，岛屿多边形参与缓冲区边界的构成，重叠多边形不参与缓冲区边界的构成； 
5. 缓冲区边界的形成，具体是将重叠区域进行合并，绘制外围的边线。

# clone

深拷贝GeoJSON对象。

思路：GeoJSON如果是Geometry格式的，那么拷贝最重要的是需要拷贝坐标信息。

```javascript
// geom是克隆的要素，geometry是被克隆的要素
geom.coordinates = deepSlice(geometry.coordinates);

// 递归拷贝坐标数组
function deepSlice(coords) {
    const cloned = coords;
    // 这里cloned[0] !== "object"为了排除等于数组的可能，而不是对象
    if (typeof cloned[0] !== "object") { return cloned.slice(); }
    // 遍历数组所有要素，使用递归的方法深度复制
    return cloned.map((coord) => {
        return deepSlice(coord);
    });
}
```

# concave

根据给定的点构造凹面外壳

**实现过程**

* 首先根据给定的点生成TIN
* 获取TIN边界

这个算法的重点在于TIN算法，可参考TIN算法

# convex

根据给定的点构造凸面外壳

**实现思路**

* 根据给定的点生成凹面外壳

  >  注意：生成的凹面外壳算法并不是concave中采用的算法，而是concaveman算法。

* 根据凹面外壳生成多边形

可以看出，这个算法中最重要的部分莫过于concaveman算法了，这是一个开源的算法，[源码](https://g.ithub.com/mapbox/concaveman)。

那么concaveman算法是如果实现的呢？

1、首先生成一个简单的凸壳多边形

```javascript
// 快速生成凸壳多边形
function fastConvexHull(points) {
    var left = points[0];
    var top = points[0];
    var right = points[0];
    var bottom = points[0];

    // 首先查找上下左右极值点
    for (var i = 0; i < points.length; i++) {
        var p = points[i];
        if (p[0] < left[0]) left = p;
        if (p[0] > right[0]) right = p;
        if (p[1] < top[1]) top = p;
        if (p[1] > bottom[1]) bottom = p;
    }

    // 将四个极值点连在一起生成一个四边形，遍历所有的点，获取在四边形外围的点
    // 为什么要获取四边形外围的点呢，因为观察凸壳多边形可以看到，所有的点（除了四个极值点）都是在四边形外围的
    var cull = [left, top, right, bottom];
    var filtered = cull.slice();
    for (i = 0; i < points.length; i++) {
        if (!pointInPolygon(points[i], cull)) filtered.push(points[i]);
    }

    return convexHull(filtered);
}

// 根据所有四边形外围的点生成凸壳多边形
// 设想通过最左点和最右点的连线将凸壳多边形分为上下两个部分，那么我们会看到每个部分如果按照横坐标遍历的话
// 线段的偏转方向是一定的，不是顺时针就是逆时针。根据这个特点可以排除那些不符合要求的点。
function convexHull(points) {
    points.sort(compareByX);

    var lower = [];
    for (var i = 0; i < points.length; i++) {
        while (lower.length >= 2 && cross(lower[lower.length - 2], lower[lower.length - 1], points[i]) <= 0) {
            lower.pop();
        }
        lower.push(points[i]);
    }

    var upper = [];
    for (var ii = points.length - 1; ii >= 0; ii--) {
        while (upper.length >= 2 && cross(upper[upper.length - 2], upper[upper.length - 1], points[ii]) <= 0) {
            upper.pop();
        }
        upper.push(points[ii]);
    }

    upper.pop();
    lower.pop();
    return lower.concat(upper);
}
```

2、根据凸壳多边形生成凹壳多边形

凸壳多边形边界有一定的规律，所以生成相对容易。可是凹壳多边形却不是这样，因为凹壳多边形是没有明显的规律的，凹壳里面也有凸壳的部分。

在所难免的，需要遍历所有的点去生成凹壳多边形，判断的依据是`该点与已知点的距离小于阈值`

海量点逐点遍历的效率是很低下的，这里引入了R树去构建空间索引。通过空间索引可大大提高空间查询的效率。



# difference

获取两个多边形不同的部分，即两个多边形空间上的相减得到的结果。

turf引用`martinez-polygon-clipping`算法实现多边形布尔运算，这是一个开源算法

[源码地址](https://github.com/w8r/martinez), [论文地址](https://www.sciencedirect.com/science/article/pii/S0965997813000379#s0030)

论文为英文，感兴趣可以看看实现的原理。

该算法使用的是论文中的P-Q类型，martinez算法提供的类型有：

![](../Image/difference1.jpg)

# dissolve

溶解要素集合为一个个的要素，通过属性过滤

实现思路：

​	将要素集中所有属性值相同的要素合并，可将操作分为两个部分，1）查找所有的属性值相同的要素 2) 将查找到的要素合并。对于第一点，通常会使用过滤数组的方式来得到，如果数据量小我想这种方法也是可以的。在turf里面使用了空间索引R树，这主要是对于大量的空间数据集。对于第二点，参考union方法

# intersect

获取两个要素的交集, 算法同union

# lineOffset

偏移线段

实现过程：

1. 获取每条线段的平行线

   ```js
   /**
    * 获取线段AB(point1, point2)距离为offset的平行线
    **/
   function processSegment(point1, point2, offset) {
       // 获取AB长度
       var L = Math.sqrt((point1[0] - point2[0]) * (point1[0] - point2[0]) + (point1[1] - point2[1]) * (point1[1] - point2[1]));
   	// 依次求出A'(out1x, out1y), B'(out2x, out2y)
       var out1x = point1[0] + offset * (point2[1] - point1[1]) / L;
       var out2x = point2[0] + offset * (point2[1] - point1[1]) / L;
       var out1y = point1[1] + offset * (point1[0] - point2[0]) / L;
       var out2y = point2[1] + offset * (point1[0] - point2[0]) / L;
       return [[out1x, out1y], [out2x, out2y]];
   }
   ```

   

2. 对组合的平行线做处理

   * 如果连续的两条平行线段之间有相交，则使用交点
   * 如果连续的两条平行线段之间不是首位相连，turf在这里没有做处理

# simplify

> 几何简化，采用了道格拉斯-普克算法，实现的JS库为[simplify-js](https://github.com/mourner/simplify-js)

道格拉斯-普克算法的思路：

​	对每一条曲线的首末点虚连一条直线,求所有点与直线的距离,并找出最大距离值*d*max ,用*d*max与限差*D*相比:若*d*max <*D*,这条曲线上的中间点全部舍去;若*d*max ≥*D*,保留*d*max 对应的坐标点,并页嚷戏欠以该点为界,把曲线分为两部分,对这两部分重复使用该方法。

实现：

```javascript
/**
 * 道格拉斯-普克算法的实现过程
 * @param {*} points 将要简化的点集
 * @param {*} first 点集中第一个元素的序号,即为0
 * @param {*} last 点集中最后一个元素的序号
 * @param {*} sqTolerance 限差
 * @param {*} simplified 结果数组
 */
function simplifyDPStep(points, first, last, sqTolerance, simplified) {
    var maxSqDist = sqTolerance; // 限差
    var index;// 游标

    for (var i = first + 1; i < last; i++) {
        // 获取点到首末两点相连线段的距离
        var sqDist = getSqSegDist(points[i], points[first], points[last]);
        // 如果距离大于限差，记录当前点的位置和距离
        if (sqDist > maxSqDist) {
            index = i;
            maxSqDist = sqDist;
        }
    }

    // 点到线段的距离大于限差，则该点满足要求并递归执行截成的两段
    if (maxSqDist > sqTolerance) {
        if (index - first > 1) simplifyDPStep(points, first, index, sqTolerance, simplified);
        simplified.push(points[index]);
        if (last - index > 1) simplifyDPStep(points, index, last, sqTolerance, simplified);
    }
}
```

# tesselate

> 细分算法，其实就是讲一个多边形分成若干个三角形。这里形成的三角网的方式不同于TIN中的delaunay三角网，这里可以形成细长型的三角形。使用的算法是mapbox中的earcut算法，该算法实现三角网更为快速。

earcut算法的实现过程：

![](../Image/earcut.png)

如上图所示，任何一个多边形都是由多个凸出三角形组成的，节点127组成一个凸三角形(节点17的连线与多边形没有任何交点)。通过遍历所有的节点，找出所有的凸三角形，所有的凸三角形集合就是最终的三角网。



如果遍历所有的节点，依次判断当前点与前后两个点是否构成凸三角形效率有点低下，因为判断是否为凸三角形需要遍历所有的点。通过观察，可以看到所有凸三角形的凸点都在一个大于180度角的两边(节点2是凸点，角872是大于180度的角)，所以可以通过遍历大于180度角边的点来提高效率。

# transformRotate

实现思路：

 	1. 遍历所有的坐标点
 	2. 计算基准点到所有点的的角度和距离
 	3. 叠加角度，计算目标点

```javascript
coordEach(geojson, function (pointCoords) {
    // 计算初始角
    var initialAngle = rhumbBearing(pivot, pointCoords);
    // 叠加角度
    var finalAngle = initialAngle + angle;
    // 计算距离
    var distance = rhumbDistance(pivot, pointCoords);
    // 计算目标点
    var newCoords = getCoords(rhumbDestination(pivot, distance, finalAngle));
    // 赋值
    pointCoords[0] = newCoords[0];
    pointCoords[1] = newCoords[1];
});
```

# transformTranslate

> 这里的偏移距离是在横向线上的偏移，参考算法chapter1/rhumbDestination

实现思路：

​	针对点线面的空间偏移可以归结为点的偏移，因为所有的线面都是由点构成的；

实现过程：

1. 遍历所有的坐标点，给定偏移角度和偏移距离，计算偏移后的点

```javascript
// 根据距离，方位计算目标点
var newCoords = getCoords(rhumbDestination(pointCoords, distance, direction, {units: units}));
// 赋值
pointCoords[0] = newCoords[0];
pointCoords[1] = newCoords[1];
```

# transformScale

根据给定的基准点和缩放尺寸对要素缩放。

![](../Image/transformScale.png)

实现思路：

1. 遍历要素中所有的点A,B,C
2. 计算基准点到各个点的偏移角度和距离(OA,OB,OC)
3. 根据缩放尺寸2，计算目标点A', B', C'

```javascript
// 原始距离
var originalDistance = rhumbDistance(origin, coord);
// 方位
var bearing = rhumbBearing(origin, coord);
// 根据缩放尺寸计算新距离
var newDistance = originalDistance * factor;
// 计算目标底单
var newCoord = getCoords(rhumbDestination(origin, newDistance, bearing));
// 赋值
coord[0] = newCoord[0];
coord[1] = newCoord[1];
```

# union

> 多边形布尔运算中的联合运算，与此类似的还有相交(intersection), 相异(diff)，异或(xor), 采用的是 [martinez的算法](https://github.com/w8r/martinez)

martinez算法的实现思路：

1、拆分线段

2、选择线段

3、联合线段

注：website中有论文pdf

# voronoi

> 泰森多边形又叫冯洛诺伊图（Voronoi diagram），得名于Georgy Voronoi，是一组由连接两邻点线段的垂直平分线组成的连续多边形组成。一个泰森多边形内的任一点到构成该多边形的控制点的距离小于到其他多边形控制点的距离。

实现过程

1. 生成Delaunay三角网，通过任意四个控制点不共圆的规则使生成的不规则三角网尽可能地饱满
2. 连接不规则三角网中相邻三角形的外接圆圆心，形成泰森多边形

[维诺图（Voronoi Diagram）分析与实现](https://cloud.tencent.com/developer/article/1176449)

