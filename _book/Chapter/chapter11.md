# squareGrid

描述：

​	创建正方形格网

思路：

1. 根据bbox的范围和格网的尺寸计算单元格的尺寸，由于bbox给出的经纬度，所以这里计算的单元格尺寸也应该用度表示
2. 遍历行数和列数，组装polygon

实现：

```javascript
    const results = [];
    const west = bbox[0];
    const south = bbox[1];
    const east = bbox[2];
    const north = bbox[3];

    // 计算单元格尺寸，以度表示
    const xFraction = cellWidth / (distance([west, south], [east, south], options));
    const cellWidthDeg = xFraction * (east - west);
    const yFraction = cellHeight / (distance([west, south], [west, north], options));
    const cellHeightDeg = yFraction * (north - south);

    // 根据bbox的长宽和单元格的尺寸计算行数和列数
    const bboxWidth = (east - west);
    const bboxHeight = (north - south);
    const columns = Math.floor(bboxWidth / cellWidthDeg);
    const rows = Math.floor(bboxHeight / cellHeightDeg);

    // 微调计算行数和列数出现的精度丢失
    const deltaX = (bboxWidth - columns * cellWidthDeg) / 2;
    const deltaY = (bboxHeight - rows * cellHeightDeg) / 2;

    // 遍历行数和列数，组装polygon
    let currentX = west + deltaX;
    for (let column = 0; column < columns; column++) {
        let currentY = south + deltaY;
        for (let row = 0; row < rows; row++) {
            const cellPoly = polygon([[
                [currentX, currentY],
                [currentX, currentY + cellHeightDeg],
                [currentX + cellWidthDeg, currentY + cellHeightDeg],
                [currentX + cellWidthDeg, currentY],
                [currentX, currentY],
            ]], options.properties);
            if (options.mask) {
                if (intersect(options.mask, cellPoly)) { results.push(cellPoly); }
            } else {
                results.push(cellPoly);
            }

            currentY += cellHeightDeg;
        }
        currentX += cellWidthDeg;
    }
    return featureCollection(results);
```

# hexGrid

描述：

​	创建正六边形网格

实现：

```javascript
/**
 * 创建六边形
 *
 * @private
 * @param {Array<number>} center 六边形中心
 * @param {number} rx 六边形宽度一半
 * @param {number} ry 六边形高度一半
 * @param {Object} properties 属性值
 * @param {Array<number>} cosines 正弦数组
 * @param {Array<number>} sines 余弦数组
 * @returns {Feature<Polygon>} hexagon
 */
function hexagon(center, rx, ry, properties, cosines, sines) {
    const vertices = [];
    for (let i = 0; i < 6; i++) {
        const x = center[0] + rx * cosines[i];
        const y = center[1] + ry * sines[i];
        vertices.push([x, y]);
    }
    //闭合
    vertices.push(vertices[0].slice());
    return polygon([vertices], properties);
}

const cosines = [];
const sines = [];
for (let i = 0; i < 6; i++) {
    const angle = 2 * Math.PI / 6 * i;
    cosines.push(Math.cos(angle));
    sines.push(Math.sin(angle));
}
```

# pointGrid

描述：

​	创建marker网格，参考[squareGrid]

# triangleGrid

描述：

​	创建三角形网格，参考[hexGrid]