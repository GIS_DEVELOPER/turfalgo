# interpolate

> IDW(反距离权重)插值

**思路：** 首先需要根据bbox生成插值的格网，然后依次遍历格网，使用IDW方法计算点集在该格网的插值结果。

**IDW算法：**

点集V{v1, ... vn},   那么插值点P的值可表示为：

![](../Image/interpolation1.png)

di表示点P与点vi的距离，p代表插值系数

**实现：**

```javascript
var results = [];
featureEach(grid, function (gridFeature) {
    var zw = 0;
    var sw = 0;
    // calculate the distance from each input point to the grid points
    featureEach(points, function (point) {
        var gridPoint = (gridType === 'point') ? gridFeature : centroid(gridFeature);
        var d = distance(gridPoint, point, options);
        var zValue;
        // property has priority for zValue, fallbacks to 3rd coordinate from geometry
        if (property !== undefined) zValue = point.properties[property];
        if (zValue === undefined) zValue = point.geometry.coordinates[2];
        if (zValue === undefined) throw new Error('zValue is missing');
        if (d === 0) zw = zValue;
        var w = 1.0 / Math.pow(d, weight);
        sw += w;
        zw += w * zValue;
    });
    // write interpolated value for each grid point
    var newFeature = clone(gridFeature);
    newFeature.properties[property] = zw / sw;
    results.push(newFeature);
});
return featureCollection(results);
```



# isobands，isolines

> 等值面和等值线算法，采用的是开源的[MarchingSquares算法](https://github.com/RaumZeit/MarchingSquares.js)，没有深究，能力有限。

![](../Image/isobands.png)



# planepoint

> 获取三角形内部某点的插值结果

**实现过程：**

```javascript	
  var x = coord[0];
  var y = coord[1];
  // 三角形第一个点的xy坐标和属性值z
  var x1 = outer[0][0];
  var y1 = outer[0][1];
  var z1 = a !== undefined ? a : outer[0][2];

  // 三角形第二个点的xy坐标和属性值z
  var x2 = outer[1][0];
  var y2 = outer[1][1];
  var z2 = b !== undefined ? b : outer[1][2];

  // 三角形第三个点的xy坐标和属性值z
  var x3 = outer[2][0];
  var y3 = outer[2][1];
  var z3 = c !== undefined ? c : outer[2][2];

  // 三点坐标IDW插值
  var z =
    (z3 * (x - x1) * (y - y2) +
      z1 * (x - x2) * (y - y3) +
      z2 * (x - x3) * (y - y1) -
      z2 * (x - x1) * (y - y3) -
      z3 * (x - x2) * (y - y1) -
      z1 * (x - x3) * (y - y2)) /
    ((x - x1) * (y - y2) +
      (x - x2) * (y - y3) +
      (x - x3) * (y - y1) -
      (x - x1) * (y - y3) -
      (x - x2) * (y - y1) -
      (x - x3) * (y - y2));
```



# tin

> 不规则三角网：模型采用一系列相连接的三角形拟合地表或其他不规则表面，常用来构造数字地面模型，特别是数字高程模型。也可以用来模拟气压、气温、磁场等地理现象。

根据一组点数据，获取不规则三角网的多边形数据

**实现过程：**

我们的输入是一组点数据，获取的是多边形数据，而多边形数据中包含的都是三角形。所以可以有以下两个步骤：

1、将一组点生成delaunay三角形

2、将生成的三角形组合成为多边形

百度文庫有相關介紹，但是不是特別清晰，https://wenku.baidu.com/view/6376d8f29e31433239689309.html