# kinks

> 获取折线或者多边形自交节点组合，本质为求线段AB和线段CD的交点坐标。算法涉及1)行列式求解方程式，2)行列式求解两条线段交点3)判断点在线段，不及深究，仅提供两个算法参考。

[行列式求解方程式](https://ccjou.wordpress.com/2014/03/05/%E5%88%A9%E7%94%A8%E8%A1%8C%E5%88%97%E5%BC%8F%E6%B1%82%E7%9B%B4%E7%B7%9A%E3%80%81%E5%B9%B3%E9%9D%A2%E5%92%8C%E5%9C%93%E6%96%B9%E7%A8%8B%E5%BC%8F/)

[行列式求解两条直线的交点](https://www.cnblogs.com/xpvincent/p/5208994.html)

[判断点在线段上](https://www.itdaan.com/tw/3391224e3d934aedcabe24c8f851adc1)

直线L1：ax + by = e

直线L2：cx + dy = f

则交点P(m, n)

​	m = (de - bf) / (ad - bc)

​	n = (af - ce) / (ad -bc)

# lineArc

> 给定圆形O, 半径R，起点角度A1，终点角度A2，生成组成圆弧的折线段L

**思路：**

1. 给定圆心和半径，可以求出该圆上所有的点(采用destination算法)，取出位于起点角度A1和终点角度A2之间的点即可

**实现：**

```javascript
/**
 * @name lineArc
 * @param {Coord} center 圆心O
 * @param {number} radius 半径R
 * @param {number} bearing1 起点角度A1
 * @param {number} bearing2 终点角度A2
 * @param {Object} [options={}] 额外的属性
 * @param {number} [options.steps=64] 精细度
 * @param {string} [options.units='kilometers'] 单位
 */
function lineArc(center: Coord, radius: number, bearing1: number, bearing2: number, options: {
    steps?: number,
    units?: Units,
} = {}): Feature<LineString> {
    // 默认折线的数目是64
    const steps = options.steps || 64;

    // 弧度转角度
    const angle1 = convertAngleTo360(bearing1);
    const angle2 = convertAngleTo360(bearing2);
    const properties = (!Array.isArray(center) && center.type === "Feature")  ? center.properties : {};

    // 初始角度A1与结束角度A2相等，则返回圆形
    if (angle1 === angle2) {
        return lineString(circle(center, radius, options).geometry.coordinates[0], properties);
    }
    const arcStartDegree = angle1;
    const arcEndDegree = (angle1 < angle2) ? angle2 : angle2 + 360;

    let alfa = arcStartDegree;
    const coordinates = [];
    let i = 0;

	// 开始角度递增，存储满足条件的节点
    while (alfa < arcEndDegree) {
        coordinates.push(destination(center, radius, alfa, options).geometry.coordinates);
        i++;
        alfa = arcStartDegree + i * 360 / steps;
    }
    if (alfa > arcEndDegree) {
        coordinates.push(destination(center, radius, arcEndDegree, options).geometry.coordinates);
    }

	// 将结果数组转为折线段L
    return lineString(coordinates, properties);
}
```



# lineChunk

> 将折线段L拆分为长度为m(m一般远小于L的长度)的许多小的折线段

**思路：**

1. 根据线段L的长度和小折线段的长m获取折线段的数目
2. 采用lineSliceAlong算法将各个小折线段存入数组中
3. 将结果数组组合成featureCollection

**实现：**

```javascript
var chunk = []; // 结果数组
// 获取线段L的长度
var lineLength = length(line, {units: units});

if (lineLength <= segmentLength) return;

// 将要裁成小线段的数目
var numberOfSegments = lineLength / segmentLength;
if (!Number.isInteger(numberOfSegments)) {
    numberOfSegments = Math.floor(numberOfSegments) + 1;
}

// 循环获取小线段
for (var i = 0; i < numberOfSegments; i++) {
    var outline = lineSliceAlong(line, segmentLength * i, segmentLength * (i + 1), {units: units});
    chunk.push(outline);
}
return featureCollection(chunk)
```





# lineIntersect

> 返回折线折线或折线多边形或多边形多边形的几何交点。多边形和折线都是由线段组成，这里给出线段交点的算法。

**实现:**

![](../Image/interestLine.png)

```javascript
// AB向量与CD向量的叉积
const denom = ((y4 - y3) * (x2 - x1)) - ((x4 - x3) * (y2 - y1));
// CA向量与CD向量的叉积
const numeA = ((x4 - x3) * (y1 - y3)) - ((y4 - y3) * (x1 - x3));
// AB向量与AC向量的叉积
const numeB = ((x2 - x1) * (y1 - y3)) - ((y2 - y1) * (x1 - x3));

const uA = numeA / denom;
const uB = numeB / denom;

// 满足条件则两条线段存在交点
if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
    const x = x1 + (uA * (x2 - x1));
    const y = y1 + (uA * (y2 - y1));
    return point([x, y]);
}
```

上面的算法其实不是很明白，如有清楚请告知，及时补充。

lineOverlap

# lineSegment

> 通常是将一条折线变成若干条线段

**思路：**

1. 遍历折线，将各个线段存储在结果数组中

**实现:**

```javascript
// 结果数组
const segments: Array<Feature<LineString>> = [];

// 遍历折线坐标
coords.reduce((previousCoords, currentCoords) => {
    // 这里会存在一条首尾坐标相同的线段，显示不受影响，其实可以做一个判断去除这个影响
    const segment = lineString([previousCoords, currentCoords], properties);
    segment.bbox = bbox(previousCoords, currentCoords);
    segments.push(segment);
    return currentCoords;
});
return segments
```



# lineSlice

>获取线段L上距输入的两点A, B最近的两点A', B'（如果A, B在线段上则A, A'相等，B, B‘ 相等）组成的折线段A'B'

**思路：**

1.  获取A', B'坐标，采用算法 nearestPointOnLine
2.  遍历线段坐标，将序号位于A', B'序号之间的节点存入结果数组
3.  将结果数组组成折线段

**实现：**

```javascript
/* 
 * @name lineSlice
 * @param {Coord} startPt A坐标
 * @param {Coord} stopPt B坐标
 * @param {Feature<LineString>|LineString} 线段L
 * @returns {Feature<LineString>} 结果折线A'B'
 */
function lineSlice(startPt, stopPt, line) {
    // 获取GeoJSON中的coordinates
    var coords = getCoords(line);
    if (getType(line) !== 'LineString') throw new Error('line must be a LineString');

    // 获取A', B'节点，采用nearestPointOnLine算法得到的结果节点会保存该节点在线段坐标中的序号index
    var startVertex = nearestPointOnLine(line, startPt);
    var stopVertex = nearestPointOnLine(line, stopPt);
    var ends;
    if (startVertex.properties.index <= stopVertex.properties.index) {
        ends = [startVertex, stopVertex];
    } else {
        ends = [stopVertex, startVertex];
    }
    var clipCoords = [ends[0].geometry.coordinates];
    
    // 获取线段上位于首末节点坐标序号之间的所有节点
    for (var i = ends[0].properties.index + 1; i < ends[1].properties.index + 1; i++) {
        clipCoords.push(coords[i]);
    }
    clipCoords.push(ends[1].geometry.coordinates);
    return linestring(clipCoords, line.properties);
}
```



# lineSliceAlong

>有点类似lineSlice，这里给定两个距离L1, L2, 获取折线段L上从起点算起距离为L1， L2的两个点A， B组成的折线段AB

**思路：**

1. 遍历折线段L节点，记录遍历距离，结果数组依次存储遍历的距离位于L1， L2之间的节点坐标
2. 将结果数组转成折线段AB

**实现：**

```javascript
/**
 * @name lineSliceAlong
 * @param {Feature<LineString>|LineString} 线段L
 * @param {number} startDist 开始距离L1
 * @param {number} stopDist 结束距离L2
 * @param {Object} [options={}] 额外的参数
 * @param {string} [options.units='kilometers'] 单位
 * @returns {Feature<LineString>} 结果折线段AB
 */
function lineSliceAlong(line, startDist, stopDist, options) {
    // 参数判断
    options = options || {};
    if (!isObject(options)) throw new Error('options is invalid');

    var coords;     // 线段L坐标
    var slice = []; // 结果数组

    // 验证
    if (line.type === 'Feature') coords = line.geometry.coordinates;
    else if (line.type === 'LineString') coords = line.coordinates;
    else throw new Error('input must be a LineString Feature or Geometry');
    var origCoordsLength = coords.length
    var travelled = 0;
    var overshot, direction, interpolated;
    
    // 遍历折线L的数组，判断遍历的距离
    for (var i = 0; i < coords.length; i++) {
        if (startDist >= travelled && i === coords.length - 1) break;
        // 结果数组添加第一个坐标，遍历的长度超出开始距离L1，折返获取第一个坐标
        else if (travelled > startDist && slice.length === 0) {
            overshot = startDist - travelled;
            direction = bearing(coords[i], coords[i - 1]) - 180;
            interpolated = destination(coords[i], overshot, direction, options);
            slice.push(interpolated.geometry.coordinates);
        }

        // 结果数组添加最后一个坐标，遍历的长度超出结束距离L2，折返获取第一个坐标
        if (travelled >= stopDist) {
            overshot = stopDist - travelled;
            direction = bearing(coords[i], coords[i - 1]) - 180;
            interpolated = destination(coords[i], overshot, direction, options);
            slice.push(interpolated.geometry.coordinates);
            return lineString(slice);
        }

        if (travelled >= startDist) {
            slice.push(coords[i]);
        }

        if (i === coords.length - 1) {
            return lineString(slice);
        }

        // 遍历距离自增
        travelled += distance(coords[i], coords[i + 1], options);
    }

    if (travelled < startDist && coords.length === origCoordsLength) throw new Error('Start position is beyond line');
    return lineString(coords[coords.length - 1]);
}
```



# lineSplit

> 使用要素F裁剪折线L，这里的要素F可以是点线面，如果是线或面的话，可以看成是F与L的交点裁剪L。所以这里考虑简单的情况，折线上的一点P裁剪这条折线L。

**思路：**

1. 折线L构建R树，方便大数据量空间查找

2. R树查找折线L中与裁剪点相交的线段I
3. 遍历折线L中线段，根据线段I的编号将折线L中的线段分为两段
4. 舍弃线段I，使用裁剪点P坐标连接

**实现：**

```javascript
/**
 * Split LineString with Point
 *
 * @private
 * @param {Feature<LineString>} line LineString
 * @param {Feature<Point>} splitter Point
 * @returns {FeatureCollection<LineString>} split LineStrings
 */
function splitLineWithPoint(line, splitter) {
    var results = [];

    // handle endpoints
    var startPoint = getCoords(line)[0];
    var endPoint = getCoords(line)[line.geometry.coordinates.length - 1];
    if (pointsEquals(startPoint, getCoord(splitter)) ||
        pointsEquals(endPoint, getCoord(splitter))) return featureCollection([line]);

    // Create spatial index
    var tree = rbush();
    var segments = lineSegment(line);
    tree.load(segments);

    // Find all segments that are within bbox of splitter
    var search = tree.search(splitter);

    // Return itself if point is not within spatial index
    if (!search.features.length) return featureCollection([line]);

    // RBush might return multiple lines - only process the closest line to splitter
    var closestSegment = findClosestFeature(splitter, search);

    // Initial value is the first point of the first segments (beginning of line)
    var initialValue = [startPoint];
    var lastCoords = featureReduce(segments, function (previous, current, index) {
        var currentCoords = getCoords(current)[1];
        var splitterCoords = getCoord(splitter);

        // Location where segment intersects with line
        if (index === closestSegment.id) {
            previous.push(splitterCoords);
            results.push(lineString(previous));
            // Don't duplicate splitter coordinate (Issue #688)
            if (pointsEquals(splitterCoords, currentCoords)) return [splitterCoords];
            return [splitterCoords, currentCoords];

        // Keep iterating over coords until finished or intersection is found
        } else {
            previous.push(currentCoords);
            return previous;
        }
    }, initialValue);
    // Append last line to final split results
    if (lastCoords.length > 1) {
        results.push(lineString(lastCoords));
    }
    return featureCollection(results);
}
```

# mask

> 返回带有内环的多边形

# nearestPointOnLine

> 获取折线L上距点P最近的点坐标

**思路：** 

1. 点P在折线L上时，P点坐标即所求坐标
2. 点P在折线L外是，所求点的坐标有两种情况：1）该点是折线L的一个端点 2）该点在折线L某条线段上(垂足)

**实现:**

```javascript
      /* 计算垂足的坐标*/

​      const start = point(coords[i]);  // 线段起点

​      start.properties.dist = distance(pt, start, options); // pt是点P

​      const stop = point(coords[i + 1]); // 线段终点

​      stop.properties.dist = distance(pt, stop, options);

​      const sectionLength = distance(start, stop, options); // 线段长度

​      const heightDistance = Math.max(start.properties.dist, stop.properties.dist);

​      const direction = bearing(start, stop);  // 计算线段方位

​	  // 获取点P与线段的两条垂线，heightDistance采用较长距离是确保垂线一定会与线段相交

​      const perpendicularPt1 = destination(pt, heightDistance, direction + 90, options); 

​      const perpendicularPt2 = destination(pt, heightDistance, direction - 90, options);

​	  // 获取垂足

​      const intersect = lineIntersects(

    ​        lineString([perpendicularPt1.geometry.coordinates, perpendicularPt2.geometry.coordinates]),

    ​        lineString([start.geometry.coordinates, stop.geometry.coordinates])

​      );
```



# shortestPath

A*算法， 图论