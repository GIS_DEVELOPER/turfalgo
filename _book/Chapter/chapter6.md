# GeoJSON对象

* Geometry：几何对象，存储坐标
* Feature：要素对象
* FeatureCollection：要素对象集合

这三种对象有包含关系，Feature对象的geometry属性是Geometry对象；FeatureCollection对象的features属性是Feature对象的集合，下面看详细看这三种对象



### 1、Geometry对象

geometry对象分为

* Point

  ```json
  {
      "type": "Point",
      "coordinates": [100.0, 0.0]
  }
  ```

* MultiPoint

  ```json
  {
      "type": "MultiPoint",
      "coordinates": [
          [100.0, 0.0],
          [101.0, 1.0]
      ]
  }
  ```

  

* LineString

  ```json
  {
      "type": "LineString",
      "coordinates": [
          [100.0, 0.0],
          [101.0, 1.0]
      ]
  }
  ```

* MultiLineString

  ```json
  {
      "type": "MultiLineString",
      "coordinates": [
          [
              [100.0, 0.0],
              [101.0, 1.0]
          ],
          [
              [102.0, 2.0],
              [103.0, 3.0]
          ]
      ]
  }
  ```

* Polygon

  Polygon的coordinates是一个三维数组，存储坐标用二维不就可以了，为什么要三维哩，这是考虑到有镂空多边形的情况

  ```json
  {
      "type": "Polygon",
      "coordinates": [
          [
              [100.0, 0.0],
              [101.0, 0.0],
              [101.0, 1.0],
              [100.0, 1.0],
              [100.0, 0.0]
          ]
      ]
  }
  
  // 镂空多边形
  {
      "type": "Polygon",
      "coordinates": [
          [
              [100.0, 0.0],
              [101.0, 0.0],
              [101.0, 1.0],
              [100.0, 1.0],
              [100.0, 0.0]
          ],
          [
              [100.8, 0.8],
              [100.8, 0.2],
              [100.2, 0.2],
              [100.2, 0.8],
              [100.8, 0.8]
          ]
      ]
  }
  ```

  

* MultiPolygon

  ```json
  {
      "type": "MultiPolygon",
      "coordinates": [
          [
              [
                  [102.0, 2.0],
                  [103.0, 2.0],
                  [103.0, 3.0],
                  [102.0, 3.0],
                  [102.0, 2.0]
              ]
          ],
          [
              [
                  [100.0, 0.0],
                  [101.0, 0.0],
                  [101.0, 1.0],
                  [100.0, 1.0],
                  [100.0, 0.0]
              ],
              [
                  [100.2, 0.2],
                  [100.2, 0.8],
                  [100.8, 0.8],
                  [100.8, 0.2],
                  [100.2, 0.2]
              ]
          ]
      ]
  }
  ```

  

* GeometryCollection

  ```json
  
  {
      "type": "GeometryCollection",
      "geometries": [{
          "type": "Point",
          "coordinates": [100.0, 0.0]
      }, {
          "type": "LineString",
          "coordinates": [
              [101.0, 0.0],
              [102.0, 1.0]
          ]
      }]
  }
  ```

Geometry对象如果存在一条线跨越日界线，则分开写，如一条线从170°E 45°N到170°W 45°N，则写成

```json
{
    "type": "MultiLineString",
    "coordinates": [
        [
            [170.0, 45.0], [180.0, 45.0]
        ], [
            [-180.0, 45.0], [-170.0, 45.0]
        ]
    ]
}
```



### 2、Feature对象

Feature格式为

```json
{
    "type": "Feature", // 固定为feature
    "geometry": {
        "type": "Point",
        "coordinates": [100.0, 0.0]
    },
    "properties":{
        name: 'balabala'
    }, 
    "id": 0  // string / number
}
```

### 3、FeatureCollection对象

FeatureCollection格式为

```json
{
    type: "FeatureCollection",
    features: [
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [100.0, 0.0]
            },
            "properties":{
                name: 'balabala'
            }, 
            "id": 0
        }
    ]
}
```

