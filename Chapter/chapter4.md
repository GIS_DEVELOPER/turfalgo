# combine

组合要素，将Point，LineString，Polygon转为MultiPoint，MultiString，MultiPolygon。

我们先来看不同要素的格式：

| 要素类型        | coordinates格式                                              |
| --------------- | ------------------------------------------------------------ |
| Point           | 一维数组，[121, 30]                                          |
| MultiPoint      | 二维数组，[[121, 30],[121,31]]                               |
| LineString      | 二维数组，[[170.0, 45.0], [180.0, 45.0]]                     |
| MultiLineString | 三维数组，[[[170.0, 45.0], [180.0, 45.0]], [ [-180.0, 45.0], [-170.0, 45.0]]] |
| Polygon         | 三维数组，[[[180.0, 40.0], [180.0, 50.0], [170.0, 50.0], [170.0, 40.0], [180.0, 40.0]               ]] |
| MultiPolygon    | 四位数组，[[[[180.0, 40.0], [180.0, 50.0], [170.0, 50.0],[170.0, 40.0], [180.0, 40.0]]            ],[[[-170.0, 40.0], [-170.0, 50.0], [-180.0, 50.0],[-180.0, 40.0], [-170.0, 40.0]]]] |

组合要素的过程就是构建新数组的过程。

# explode

将几何要素转变为最小组成单元即点的集合。

# flatten

将要素扁平化处理

* MultiPoint  => Point的集合
* MultiLineString => LineString 的集合
* MultiPolygon => Polygon的集合

# lineToPolygon

将折线转为多边形。

这里的折线坐标会做闭合操作，即使最后一个点坐标和第一个点坐标相等。

```javascript
// 折线坐标
[
    [180.0, 40.0], [180.0, 50.0], [170.0, 50.0],
    [170.0, 40.0]
]

// 折线需要添加一个点
[
    [
        [180.0, 40.0], [180.0, 50.0], [170.0, 50.0],
        [170.0, 40.0], [180.0, 40.0]
    ]
]
```

# polygonize

多边形化

实现思路：

​	将输入的几何要素(这里多指线段）打散为点集，然后将点集构造成图结构，最后通过图获取外围多边形



# polygonToLine

多边形折线化

**实现:**

```javascript
// Polygon的coordinates属性是一个三维数组，

// 如果数组的长度大于一则表示Polygon拥有孔
// multiLineString是使用坐标构造MultiLineString
if (coords.length > 1) { return multiLineString(coords, properties); }
// lineString使用坐标构造lineString
return lineString(coords[0], properties);
```

