# coordAll

获取geojson对象中coordinate坐标

# coordEach

遍历geojson坐标，执行操作

# coordReduce

遍历geojson坐标，执行操作，与coordEach差别在于后者是每个都操作，而前者是累计操作，返回一个结果



# featureEach

遍历geojson中feature，执行操作

# featureReduce

遍历geojson中feature，执行操作

# flattenEach

遍历扁平化处理后的要素，执行操作，扁平化处理即使多层次复杂的要素变为一个个单独的要素。

# flattenReduce

遍历扁平化处理后的要素，执行操作

# getCoord

获取单个几何要素坐标

# getCoords

获取多个几何要素坐标

# getGeom

获取geometry

# getType

获取要素类型

# geomEach

遍历geometry，执行操作

# geomReduce

遍历geometry，执行操作

# propEach

遍历属性，执行操作

# propReduce

遍历属性执行操作

# segmentEach

遍历线段，执行操作

# segmentReduce

遍历线段，执行操作

# getCluster

获取簇

# clusterEach

获取簇，并执行操作

# clusterReduce

获取簇，并执行操作